﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class Program
    {
        private double[] array;
        private bool minElem;
        private bool maxElem;
        private bool sumElem;
        private bool sortASC;
        private bool sortDESC;
        private bool printElem;
        private bool exitProg;


        public Program(int n)
        {
            array = new double[n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                array[i] = rand.Next(0, 1000);
            }
            minElem = true;
            maxElem = true;
            sumElem = true;
            sortASC = true;
            sortDESC = true;
            printElem = true;
            exitProg = true;
        }

        public Program(double[] array1)
        {
            array = new double[array1.Length];
            for (int i = 0; i < array1.Length; i++)
            {
                array[i] = Math.Round((array1[i] + array1.Max()) / array1.Min());
            }
            minElem = true;
            maxElem = true;
            sumElem = true;
            sortASC = true;
            sortDESC = true;
            printElem = true;
            exitProg = true;
        }

        public void PrintMenu()
        {
            Console.WriteLine("Актуальное меню: ");
            if (minElem)
                Console.WriteLine("Для вывода минимального елемента введите коменду \"min\"");
            if (maxElem)
                Console.WriteLine("Для вывода максимального елемента введите коменду \"max\"");
            if (sumElem)
                Console.WriteLine("Для вывода суммы елементов введите коменду \"sum\"");
            if (sortASC)
                Console.WriteLine("Для сортировки елементов по возрастанию введите коменду \"sortAsc\"");
            if (sortDESC)
                Console.WriteLine("Для сортировки елементов по спаданию введите коменду \"sortDesc\"");
            if (printElem)
                Console.WriteLine("Для вывода всех елементов введите коменду \"print\"");
            if (exitProg)
                Console.WriteLine("Для выхода введите коменду \"exit\"");
            string command = Console.ReadLine();
            DoCommand(command);
        }

        public void DoCommand(string command)
        {
            switch (command)
            {
                case "min": PrintMin();
                    break;
                case "max": PrintMax();
                    break;
                case "sum": PrintSum();
                    break;
                case "sortAsc": SortAsc();
                    break;
                case "sortDesc": SortDesc();
                    break;
                case "print": PrintElem();
                    break;
                case "exit": break;
                default: Console.WriteLine("Неправильная комманда");
                    PrintMenu();
                    break;
            }
        }

        public void SortAsc()
        {
            if (sortASC)
            {
                double[] res = new double[array.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    res[i] = array[i];
                }
                Console.WriteLine("Сортировка по возростанию: ");
                Array.Sort(res);
                for (int i = 0; i < array.Length; i++)
                {
                    Console.Write("{0} ", res[i]);
                }
                Console.WriteLine();
            }
            sortASC = false;
            PrintMenu();
        }


        public void SortDesc()
        {
            if (sortDESC)
            {
                double[] res = new double[array.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    res[i] = array[i];
                }
                Console.WriteLine("Сортировка по спаданию: ");
                Array.Sort(res);
                Array.Reverse(res);
                for (int i = 0; i < array.Length; i++)
                {
                    Console.Write("{0} ", res[i]);
                }
                Console.WriteLine();
            }
            sortDESC = false;
            PrintMenu();
        }

        public void PrintMax()
        {
            if (maxElem)
            {
                Console.WriteLine("Максимальный елемент: ");
                Console.Write(array.Max());
                Console.WriteLine();
            }
            maxElem = false;
            PrintMenu();
        }

        public void PrintSum()
        {
            if (sumElem)
            {
                Console.WriteLine("Сумма елементов: ");
                Console.Write(array.Sum());
                Console.WriteLine();
            }
            sumElem = false;
            PrintMenu();
        }


        public void PrintMin()
        {
            if (minElem)
            {
                Console.WriteLine("Минимальный елемент: ");
                Console.Write(array.Min());
                Console.WriteLine();
            }
            minElem = false;
            PrintMenu();
        }

        public void PrintElem()
        {
            if (printElem)
            {
                Console.WriteLine("Вывод массива: ");
                for (int i = 0; i < array.Length; i++)
                {
                    double elem = array[i];
                    string res = "";
                    int temp = (int)(elem / 100);
                    if (temp >= 1)
                    {
                        if (temp == 1) res += "сто ";
                        if (temp == 2) res += "двести ";
                        if (temp == 3) res += "триста ";
                        if (temp == 4) res += "четиреста ";
                        if (temp == 5) res += "пятьсот ";
                        if (temp == 6) res += "шестьсот ";
                        if (temp == 7) res += "семьсот ";
                        if (temp == 8) res += "восемьсот ";
                        if (temp == 9) res += "девятсот ";
                        elem = elem % 100;
                    }
                    temp = (int)(elem / 10);
                    if (temp >= 2 )
                    {                  
                        if (temp == 2) res += "двадцать ";
                        if (temp == 3) res += "тридцать ";
                        if (temp == 4) res += "сорок ";
                        if (temp == 5) res += "пятдесят ";
                        if (temp == 6) res += "шестдесят ";
                        if (temp == 7) res += "семдесят ";
                        if (temp == 8) res += "восемдесят ";
                        if (temp == 9) res += "девяносто ";
                        elem = elem % 10;
                    }
                    else
                    {
                        temp = (int)elem;
                        if (temp == 10) res += "десят ";
                        if (temp == 11) res += "одинадцать";
                        if (temp == 12) res += "двенадцать";
                        if (temp == 13) res += "тринадцать";
                        if (temp == 14) res += "четырнадцать";
                        if (temp == 15) res += "пятнадцать";
                        if (temp == 16) res += "шестнадцать";
                        if (temp == 17) res += "семнадцать";
                        if (temp == 18) res += "восемнадцать";
                        if (temp == 19) res += "девятнадцать";
                    }
                    temp =(int)elem;
                    if (temp > 0 && temp < 10)
                    {
                        if (temp == 1) res += "один ";
                        if (temp == 2) res += "два ";
                        if (temp == 3) res += "три ";
                        if (temp == 4) res += "четире ";
                        if (temp == 5) res += "пять";
                        if (temp == 6) res += "шесть ";
                        if (temp == 7) res += "семь ";
                        if (temp == 8) res += "восемь ";
                        if (temp == 9) res += "девять ";
                    }
                    res += ", ";
                    Console.Write("{0} ", res);
                }
                Console.WriteLine();
            }
            printElem = false;
            PrintMenu();
        }


        static void Main(string[] args)
        {

            Console.Write("Введите количество елемнтов массива: ");
            int n;
            try
            {
                n = Convert.ToInt32(Console.ReadLine());
                Program mas = new Program(n);
                mas.PrintMenu();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("----Работа с новым массивом----");
                Program mas1 = new Program(mas.array);
                mas1.PrintMenu();
            }
            catch (FormatException)
            {
                Console.WriteLine("Неправильный формат!");
            }
                
            
        }
    }
}
